CREATE USER 'guia'@'localhost' IDENTIFIED BY 'guia';

CREATE DATABASE IF NOT EXISTS guia_quartel;

GRANT ALL ON guia_quartel.* TO 'guia'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE IF NOT EXISTS guia(
	id INT UNIQUE NOT NULL PRIMARY KEY,
	valor DECIMAL(7, 2) NOT NULL,
	secao INT NOT NULL 
) ENGINE=innodb;

