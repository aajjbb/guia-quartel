package com.guias.view;

import java.text.NumberFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.guias.dao.GuiaDao;
import com.guias.model.Guia;

public class GuiaTableModel extends AbstractTableModel{	
	private static final long serialVersionUID = 5924942813427805152L;		
	private List<Guia> data;
	private String[] colunas = {"Id","Valor"};	
	private GuiaDao dao;	
	private int secao;
	
	public GuiaTableModel(int secao) {
		this.secao = secao;
		dao = new GuiaDao();
		data = dao.retrieve(getSecao());
	}
	
	public void addRow(Guia guia) {		
		boolean done = this.dao.add(guia);
		if (done) {
			this.data.add(guia);		
			this.fireTableDataChanged();
		}
	}
	
	public void removeRow(int id) {
		boolean done = this.dao.remove(id);
		if (done) {
			this.data = this.dao.retrieve(getSecao());
			this.fireTableDataChanged();		
		}
	}

	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}

	@Override
	public int getRowCount() {
		return this.data.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) { 
		if (coluna == 0) {
			return this.data.get(linha).getId();
		} else {
			NumberFormat formater = NumberFormat.getCurrencyInstance();
			return formater.format(this.data.get(linha).getValor());
		}
	}

	public List<Guia> getData() {
		return data;
	}
	
	public int getSecao() {
		return secao;
	}
}
