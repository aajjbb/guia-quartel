package com.guias.view;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

public class Main extends JFrame {	
	private static final long serialVersionUID = -6796894851753949775L;
	private static final int SECOES = 13;	
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnArd;
	private JMenuItem mntmInserirGuia;	
	private JTable table[];
	private JButton btnInsereGuia;
	private JButton btnRemoveGuia;
	private JTabbedPane tabbedPane;
	private JPanel panelButtons;
	
	public static GuiaTableModel[] model = new GuiaTableModel[SECOES];
	/**
	 * 
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Main() {
	    Locale.setDefault(new Locale("pt", "BR"));  
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 632, 445);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnArd = new JMenu("Arquivo");
		menuBar.add(mnArd);
		
		mntmInserirGuia = new JMenuItem("Inserir Guia");
		mntmInserirGuia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GuiaForm tmp_form = new GuiaForm(tabbedPane.getSelectedIndex() + 1);
				tmp_form.setLocationRelativeTo(contentPane);
				tmp_form.setVisible(true);
			}
		});
		mnArd.add(mntmInserirGuia);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);		
		contentPane.setLayout(new BorderLayout(0, 0));		
		

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);		
		table = new JTable[SECOES];
				
		for (int i = 0; i < 13; i++) {
			model[i] = new GuiaTableModel(i + 1);
			table[i] = new JTable(model[i]);
			tabbedPane.addTab("Seção " + (i + 1), table[i]);
		}
		
		contentPane.add(tabbedPane);
		
		panelButtons = new JPanel();		
		contentPane.add(panelButtons, BorderLayout.SOUTH);		
		
		btnInsereGuia = new JButton("Insere Guia");
		btnInsereGuia.setBounds(103, 0, 115, 25);
		panelButtons.add(btnInsereGuia);
		
		btnRemoveGuia = new JButton("Remove Guia");
		btnRemoveGuia.setBounds(230, 0, 125, 25);
		panelButtons.add(btnRemoveGuia);
		
		btnRemoveGuia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pos = tabbedPane.getSelectedIndex();
				int id = model[pos].getData().get(table[pos].getSelectedRow()).getId();
				model[pos].removeRow(id);				
			}
		});
		btnInsereGuia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				GuiaForm tmp_form = new GuiaForm(tabbedPane.getSelectedIndex() + 1);
				tmp_form.setLocationRelativeTo(contentPane);
				tmp_form.setVisible(true);
			}
		});
	}
}
