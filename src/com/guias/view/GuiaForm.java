package com.guias.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import com.guias.model.Guia;

public class GuiaForm extends JDialog {	
	private static final long serialVersionUID = 8120489908895501791L;
	private final JPanel contentPanel = new JPanel();
	private JTextField fieldId;
	private JTextField fieldValor;
	private JButton buttonCancela;
	private JLabel labelValor;
	private int secao;
	
		/**
	 * Create the dialog.
	 */
	public GuiaForm(int secao) {
		this.secao = secao;
		setBounds(100, 100, 243, 211);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel labelTitulo = new JLabel("Inserção de Guia");
		labelTitulo.setBounds(12, 12, 130, 21);
		contentPanel.add(labelTitulo);
		{
			JLabel labelId = new JLabel("Número:");
			labelId.setBounds(12, 44, 70, 15);
			contentPanel.add(labelId);
		}
		{
			labelValor = new JLabel("Valor:");
			labelValor.setBounds(12, 71, 70, 15);
			contentPanel.add(labelValor);
		}
		{
			fieldId = new JTextField();
			fieldId.setBounds(81, 42, 114, 19);
			contentPanel.add(fieldId);
			fieldId.setColumns(10);
		}
		{
			fieldValor = new JTextField();
			fieldValor.setBounds(81, 71, 114, 19);
			contentPanel.add(fieldValor);
			fieldValor.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton buttonInsere = new JButton("Insere");
				buttonInsere.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int id;
						double valor;
						try {
							id = Integer.parseInt(fieldId.getText().trim());
							valor = Double.parseDouble(fieldValor.getText().trim());
							
							Main.model[getSecao() - 1].addRow(new Guia(id, valor, getSecao()));
							
							fieldId.setText("");
							fieldValor.setText("");
							
							fieldId.requestFocus();
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(null, "Há um problema no preenchimento da guia.");
						}						
					}
				});
				buttonInsere.setActionCommand("OK");
				buttonPane.add(buttonInsere);
				getRootPane().setDefaultButton(buttonInsere);
			}
			{
				buttonCancela = new JButton("Cancela");
				buttonCancela.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				buttonCancela.setActionCommand("Cancel");
				buttonPane.add(buttonCancela);
			}			
		}		
	}
	
	protected MaskFormatter createFormatter(String s) {
	    MaskFormatter formatter = null;
	    try {
	        formatter = new MaskFormatter(s);
	    } catch (java.text.ParseException exc) {
	        System.err.println("formatter is bad: " + exc.getMessage());
	        System.exit(-1);
	    }
	    return formatter;
	}	

	public int getSecao() {
		return secao;
	}
}
