package com.guias.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.guias.model.Guia;

public class GuiaDao {
	private Connection connection;

	public GuiaDao() {
		super();
		this.connection = new ConnectionFactory().getConnection();
	}

	public boolean add(final Guia guia) {	
		String sql = "INSERT INTO guia (id, valor, secao) VALUES (?, ?, ?)";
		
		System.out.print(guia);
		
		try {		
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			
			stmt.setInt(1, guia.getId());
			stmt.setDouble(2, guia.getValor());
			stmt.setInt(3, guia.getSecao());
			
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Houve um problema na conexao com o banco de dados ao inserir uma guia.");
			System.err.print(e);
			return false;
		}		
		return true;
	}
	
	public boolean remove(int id) {
		String sql = "DELETE FROM guia WHERE id = ?";
		
		try {		
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			
			stmt.setInt(1, id);			
			
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Houve um problema na conexao com o banco de dados ao remover uma.");			
			return false;
		}
		return true;
	}
	
	public List<Guia> retrieve(int secao) {
		String sql = "SELECT * FROM guia WHERE secao = ?";
		List<Guia> lista = new ArrayList<>();
		
		try {
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			
			stmt.setInt(1, secao);
			
			ResultSet rs = stmt.executeQuery();
			
			Guia buffer;
			
			while (rs.next()) {
				buffer = new Guia(rs.getInt("id"), rs.getDouble("valor"));
				lista.add(buffer);
			}			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Houve um problema na conexao com o banco de dados ao retornar as guias cadastradas.");
			return new ArrayList<Guia>();
		}
		return lista;
	}
	
	public Connection getConnection() {
		return connection;
	}	
}
