package com.guias.model;

public class Guia {
	private int id, secao;
	private double valor;	
	
	public Guia(int id, double valor, int secao) {
		super();
		this.id = id;
		this.valor = valor;
		this.secao = secao;
	}
	
	public Guia(int id, double valor) {
		super();
		this.id = id;
		this.valor = valor;
	}
	
	public Guia() {
		
	}
	
	public int getId() {
		return id;
	}
	
	public double getValor() {
		return valor;
	}

	public int getSecao() {
		return secao;
	}		
	
	@Override
	public String toString() {	
		return this.id + " " + this.valor + " " + this.secao;
	}
}
